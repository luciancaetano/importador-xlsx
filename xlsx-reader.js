#!/usr/bin/env node

"use strict";

var _ = require('underscore');
var fs = require('fs');
var path = require('path');
var VERSION = '1.0.3';


class XLXSImporter{
	/**
	* Construtor
	*/
	constructor(){
		
		this.xlsxToJson = require('xlsx2json');
		this.errors = [];
		this.result = [];
		this._h = [];
	}
	/**
	* Valida os dados
	*/
	validate(){
		if(this.fileName == undefined || this.fileName == null){
			this.error('Arquivo não informado no parâmetro',"ERR_PARAM_FILE_NULL");
		}else{
			this.file = path.resolve(this.fileName);

			if(!this.fileExists(this.file)){
				this.error('Arquivo não encontrado',"ERR_FILE_NOT_FOUND");
			}
		}
		return this.errors.length == 0;
	}
	/**
	* Processa o XLS
	*/
	startImport(file){
		var self = this;
		this.fileName = file;
		if(this.validate()){
			this.xlsxToJson(this.file).then(function(jsonArray){
				_.each(jsonArray[0], function(title , index){
					self._h.push(title);
				});
				_.each(jsonArray, function(line, index){
					if(index > 0){
						var newLine = {};
						var isEmpty = true;
						_.each(jsonArray[0], function(rows , index){
							if(line[index].length > 0){
								isEmpty = false;
							}
							newLine[rows] = line[index];
						});
						if(!isEmpty){
							self.result.push(newLine);
						}
					}
				});
				self.resume();
			});
		}else{
			self.resume();
		}		
	}
	/**
	* Registra um erro
	*/
	error(msg,code){
		code = code || NULL;
		this.errors.push({msg: msg, code: code});
	}
	/**
	 * Retorna no console o resumo
	 */
	resume(){
		if(this.errors.length > 0){
			console.log(JSON.stringify({
				status: 'error',
				reason: this.errors,
				version: VERSION
			}));
		}else{
			console.log(JSON.stringify({
				status: 'success',
				data: this.result,
				headers: this._h,
				version: VERSION
			}));
		}
	}
	/**
	 * Verifica se um arquivo existe
	 */
	fileExists(filePath)
	{
	    try{
	        return fs.statSync(filePath).isFile();
	    }catch (err){
	        return false;
	    }
	}
}

var Importer = new XLXSImporter();
Importer.startImport(process.argv[2]);
