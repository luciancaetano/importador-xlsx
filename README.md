# NodeJS XLSX Importer #
Este é um simples importador de xls o qual retorna um json
## Mode de uso ##
    xlsx-reader arquivo.xlsx
## Retorno ##
    Ao ler o xlsx com sucesso o mesmo retorna uma mensagem json na seguinte estrutura
    
```
#!javascript

{
    status: "success",
    data: ...xlsx result,
    headers: [...],
    version: '....'
}
```

    
    Ao ocorrer algum erro a seguinte estrutura json é retornada

```
#!javascript 
{
     status: "error",
     reason: [{msg: "mensagem do erro", code: "código do erro"}, ....],
     version: '....'
}
```

    
# Instalando #
Execute como root
    npm install -g https://username:password@bitbucket.org/luciancaetano/importador-xlsx.git

## Windows
Alguns servidores widnows podem pedir permissão aministrativa sendo assim alterando a localização do app data para
C:\Windows\System32\config\systemprofile\AppData\Roaming 

Sendo assim se este for o seu caso o php não irá encontrar o script.
Comesse indo em iniciar/executar e digite "%appdata%/npm" sem aspas, e copie todo o conteúdo para a pasta C:\Windows\System32\config\systemprofile\AppData\Roaming

Porém cuidado cada vez que você executar npm update você deve refazer este processo novamente para manter os seus scripts atualziados para o php